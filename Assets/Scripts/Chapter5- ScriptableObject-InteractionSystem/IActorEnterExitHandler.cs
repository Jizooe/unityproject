﻿using UnityEngine;

namespace Chapter5_ScriptableObject_InteractionSystem
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }
}