﻿namespace Chapter5_ScriptableObject_InteractionSystem
{
    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();

        void MoveBackward();

        void TurnLeft();
        void TurnRight();
    }
}