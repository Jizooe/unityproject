﻿using UnityEngine;

namespace Chapter5_ScriptableObject_InteractionSystem
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}