using System;
using UnityEngine;
namespace Chapter5_ScriptableObject_InteractionSystem
{
    public class AlwaysFaceCamera : MonoBehaviour
    {
        private void Update()
        {
            transform.rotation =
                Quaternion.LookRotation( transform.position - Camera.main.transform.position );
        }
    }
}