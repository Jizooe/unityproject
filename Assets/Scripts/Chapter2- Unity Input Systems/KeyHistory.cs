using System.Collections.Generic;
using UnityEngine;

public class KeyHistory : MonoBehaviour
{
    private float historyDuration = 10f;
    private List<KeyPress> keyHistory = new List<KeyPress>();

    private void Update()
    {
        // Remove keys from history if they exceed the history duration
        keyHistory.RemoveAll(keyPress => Time.time - keyPress.Time > historyDuration);

        // Check for new key presses
        foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(keyCode))
            {
                // Add the key press to the history
                keyHistory.Insert(0, new KeyPress(keyCode, Time.time));

                // Limit the history size to 5
                if (keyHistory.Count > 5)
                    keyHistory.RemoveAt(keyHistory.Count - 1);

                // Print the updated history
                Debug.Log("Key History: " + GetKeyHistoryString());
            }
        }
    }

    private string GetKeyHistoryString()
    {
        string historyString = "";
        foreach (KeyPress keyPress in keyHistory)
        {
            historyString += keyPress.Key.ToString() + " ";
        }
        return historyString.Trim();
    }
}

public class KeyPress
{
    public KeyCode Key { get; private set; }
    public float Time { get; private set; }

    public KeyPress(KeyCode key, float time)
    {
        Key = key;
        Time = time;
    }
}