using UnityEngine;
using UnityEngine.Audio;

namespace Chapter11_Background_Music_BGM_and_Sound_Effect_SFX_
{
    [CreateAssetMenu(menuName = "GameDev3/Lab11/Sound&Music", fileName = "SoundSettingsPreset")]
    public class SoundSettings : ScriptableObject
    {
        public AudioMixer AudioMixer;

        [Header("MasterVolume")] public string MasterVolumeName = "MasterVolume";
        [Range(-80, 20)] public float MasterVolume;

        [Header("MusicVolume")] public string MusicVolumeName = "MusicVolume";
        [Range(-80, 20)] public float MusicVolume;

        [Header("MasterSFXVolume")] public string MasterSFXVolumeName = "MasterSFXVolume";
        [Range(-80, 20)] public float MasterSFXVolume;

        [Header("SFXVolume")] public string SFXVolumeName = "SFXVolume";
        [Range(-80, 20)] public float SFXVolume;

        [Header("UIVolume")] public string UIVolumeName = "UIVolume";
        [Range(-80, 20)] public float UIVolume;
    }
}
