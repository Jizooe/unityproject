﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Chapter11_Background_Music_BGM_and_Sound_Effect_SFX_
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] protected SoundSettings m_soundSettings;

        public Slider m_SliderMasterVolume;
        public Slider m_SliderMusicVolume;
        public Slider m_SliderMasterSFXVolume;
        public Slider m_SliderSFXVolume;
        public Slider m_SliderUIVolume;

        void Start()
        {
            InitialiseVolumes();
        }

        private void InitialiseVolumes()
        {
            SetMasterVolume(m_soundSettings.MasterVolume);
            SetMusicVolume(m_soundSettings.MusicVolume);
            SetMasterSFXVolume(m_soundSettings.MasterSFXVolume);
            SetSFXVolume(m_soundSettings.SFXVolume);
            SetUIVolume(m_soundSettings.UIVolume);
        }

        public void SetMasterVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.MasterVolumeName, vol);
            m_soundSettings.MasterVolume = vol;
            m_SliderMasterVolume.value = m_soundSettings.MasterVolume;
        }

        public void SetMusicVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.MusicVolumeName, vol);
            m_soundSettings.MusicVolume = vol;
            m_SliderMusicVolume.value = m_soundSettings.MusicVolume;
        }

        public void SetMasterSFXVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.MasterSFXVolumeName, vol);
            m_soundSettings.MasterSFXVolume = vol;
            m_SliderMasterSFXVolume.value = m_soundSettings.MasterSFXVolume;
        }

        public void SetSFXVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.SFXVolumeName, vol);
            m_soundSettings.SFXVolume = vol;
            m_SliderSFXVolume.value = m_soundSettings.SFXVolume;
        }

        public void SetUIVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.UIVolumeName, vol);
            m_soundSettings.UIVolume = vol;
            m_SliderUIVolume.value = m_soundSettings.UIVolume;
        }
    }
}