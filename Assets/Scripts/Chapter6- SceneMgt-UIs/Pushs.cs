﻿using System;
using UnityEngine;
using Chapter5_ScriptableObject_InteractionSystem;
using Samarnggoon.GameDev3.Chapter1;

namespace Chapter6_SceneMgt_UIs
{
    public class Pushs : MonoBehaviour
    {
        [SerializeField] private float _Power = 10;

        private Rigidbody _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Push(GameObject actor)
        {
            _rigidbody.AddForce(actor.transform.forward * _Power,ForceMode.Impulse);
        }
    }
}