﻿using Samarnggoon.GameDev3.Chapter1;
using Chapter5_ScriptableObject_InteractionSystem;
using UnityEngine;


namespace Chapter6_SceneMgt_UIs
{
    public class Pickupable : MonoBehaviour , IInteractable ,IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
        {
            var itemTypeComponent = GetComponent <ItemTypeComponent >();
            
            var inventory = actor.GetComponent <IInventory >();
            
            inventory.AddItem(itemTypeComponent.Type.ToString(),1);
            
            Destroy(gameObject);
        }

        public void ActorEnter(GameObject actor)
        {
            
        }

        public void ActorExit(GameObject actor)
        {
            
        }
    }
}