﻿using System;
using Chapter5_ScriptableObject_InteractionSystem;
using UnityEngine;
using UnityEngine.Events;

namespace Chapter6_SceneMgt_UIs
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();
    
        [SerializeField] protected UnityEvent<GameObject> m_OnInteractGameOject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnActorEnterGameOject = new();
        [SerializeField] protected UnityEvent<GameObject> m_OnactorExitGameOject = new();

        public virtual void Interact(GameObject actor)
        {
            m_OnInteract.Invoke();
            m_OnInteractGameOject.Invoke(actor);
        }

        public virtual void ActorEnter(GameObject actor)
        {
            m_OnActorEnter.Invoke();
            m_OnActorEnterGameOject.Invoke(actor);
        }

        public virtual void ActorExit(GameObject actor)
        {
            m_OnActorExit.Invoke();
            m_OnactorExitGameOject.Invoke(actor);
        }
    }
}