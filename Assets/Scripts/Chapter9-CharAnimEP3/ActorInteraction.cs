using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Chapter5_ScriptableObject_InteractionSystem;

public class ActorInteraction : MonoBehaviour
{
    [SerializeField] protected ActorTriggerHandlerV2 m_ActorTriggerHandler;

    [SerializeField] protected Key m_InteractionKey;

    [SerializeField] protected UnityEvent m_OnStartInteract = new();

    // Start is called before the first frame update
    void Start()
    {
        if (m_ActorTriggerHandler == null)
        {
            m_ActorTriggerHandler = GetComponentInChildren<ActorTriggerHandlerV2>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Keyboard keyboard = Keyboard.current;

        if (keyboard[m_InteractionKey].wasPressedThisFrame)
        {
            PerformInteraction();
        }
    }

    //function

    protected virtual void PerformInteraction()
    {
        var interactables = m_ActorTriggerHandler.GetInteractables();

        if (interactables?.Length > 0)
        {
            m_OnStartInteract.Invoke();

            foreach (var interactble in interactables)
            {
                interactble.Interact(gameObject);
            }
        }
    }
}

