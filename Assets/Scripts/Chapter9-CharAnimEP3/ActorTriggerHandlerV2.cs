using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chapter5_ScriptableObject_InteractionSystem;

public class ActorTriggerHandlerV2 : ActorTriggerHandler
{
    public virtual IInteractable[] GetInteractables()
    {
        m_TriggeredGameObjects.RemoveAll(gameObjectject => gameObjectject == null);

        if (m_TriggeredGameObjects.Count == 0)
        {
            return null;
        }
        
        return m_TriggeredGameObjects[0].GetComponents <IInteractable >();
        
    }
}
