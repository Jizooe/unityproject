﻿using UnityEngine;

namespace Chapter9_CharAnimEP3
{
    [CreateAssetMenu]
    public class PrefabSpawner : ScriptableObject
    {
        public GameObject m_prefab;

        public void SpawnPrefab(GameObject parent) //หาพ่อให้มัน เช่น gameobjectป่าวๆ
        {
            var go = Instantiate(m_prefab);
            go.transform.position = parent.transform.position;
        }
    }
}