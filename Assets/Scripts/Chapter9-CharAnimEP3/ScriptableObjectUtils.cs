﻿using UnityEngine;

namespace Chapter9_CharAnimEP3
{
    [CreateAssetMenu]
    public class ScriptableObjectUtils : ScriptableObject
    {
        public void Destroy(GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }
    }
}